"""bureau.csv data feature engineering according to
https://www.kaggle.com/code/shanth84/home-credit-bureau-data-feature-engineering
"""
import pandas as pd
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def add_bureau_features(input_path: str, output_path: str):
    """Creating additional features and aggregating bureau data.

    :param input_path: Path to read original bureau DataFrame.
    :type input_path: str
    :param output_path: Path to save processed DataFrame.
    :type output_path: str
    :return:
    """
    if "bureau.csv" not in input_path:
        raise Exception("Input path does not contain bureau.csv file!")

    bureau = pd.read_csv(
        input_path, dtype={"SK_ID_CURR": "object", "SK_ID_BUREAU": "object"}
    )

    bureau["AMT_CREDIT_SUM_DEBT"] = bureau["AMT_CREDIT_SUM_DEBT"].fillna(0)
    bureau["AMT_CREDIT_SUM"] = bureau["AMT_CREDIT_SUM"].fillna(0)
    bureau["AMT_CREDIT_SUM_OVERDUE"] = bureau["AMT_CREDIT_SUM_OVERDUE"].fillna(0)
    bureau["CNT_CREDIT_PROLONG"] = bureau["CNT_CREDIT_PROLONG"].fillna(0)
    bureau["AMT_CREDIT_MAX_OVERDUE"] = bureau["AMT_CREDIT_MAX_OVERDUE"].fillna(0)

    bureau["CREDIT_ACTIVE_BINARY"] = bureau["CREDIT_ACTIVE"].apply(
        lambda x: 0 if x == "Closed" else 1
    )
    bureau["CREDIT_ENDDATE_BINARY"] = bureau["DAYS_CREDIT_ENDDATE"].apply(
        lambda x: 0 if x < 0 else 1
    )
    bureau["AMT_ANNUITY_BINARY"] = bureau["AMT_ANNUITY"].apply(
        lambda x: 0 if pd.isna(x) else 1
    )

    tmp1 = (
        bureau.groupby(by=["SK_ID_CURR"])
        .agg(
            {
                "DAYS_CREDIT": "count",
                "AMT_CREDIT_MAX_OVERDUE": "mean",
                "CREDIT_TYPE": "nunique",
                "CREDIT_ACTIVE_BINARY": "mean",
                "CREDIT_ENDDATE_BINARY": "mean",
                "AMT_CREDIT_SUM_DEBT": "sum",
                "AMT_CREDIT_SUM": "sum",
                "AMT_CREDIT_SUM_OVERDUE": "sum",
                "DAYS_CREDIT_UPDATE": "mean",
                "CNT_CREDIT_PROLONG": "mean",
                "AMT_ANNUITY_BINARY": "mean",
            }
        )
        .reset_index()
        .rename(
            index=str,
            columns={
                "DAYS_CREDIT": "BUREAU_LOAN_COUNT",
                "AMT_CREDIT_MAX_OVERDUE": "AVG_CREDIT_MAX_OVERDUE",
                "CREDIT_TYPE": "BUREAU_LOAN_TYPES",
                "CREDIT_ACTIVE_BINARY": "ACTIVE_LOANS_PERCENTAGE",
                "CREDIT_ENDDATE_BINARY": "CREDIT_ENDDATE_PERCENTAGE",
                "AMT_CREDIT_SUM_DEBT": "TOTAL_CUSTOMER_DEBT",
                "AMT_CREDIT_SUM": "TOTAL_CUSTOMER_CREDIT",
                "AMT_CREDIT_SUM_OVERDUE": "TOTAL_CUSTOMER_OVERDUE",
                "DAYS_CREDIT_UPDATE": "AVG_DAYS_CREDIT_UPDATE",
                "CNT_CREDIT_PROLONG": "AVG_CREDITDAYS_PROLONGED",
                "AMT_ANNUITY_BINARY": "AMT_ANNUITY_PERCENTAGE",
            },
        )
    )
    tmp1["AVERAGE_LOAN_TYPE"] = tmp1["BUREAU_LOAN_COUNT"] / tmp1["BUREAU_LOAN_TYPES"]
    tmp1["DEBT_CREDIT_RATIO"] = tmp1["TOTAL_CUSTOMER_DEBT"] / (
        tmp1["TOTAL_CUSTOMER_CREDIT"] + 1e-3
    )
    tmp1["OVERDUE_DEBT_RATIO"] = tmp1["TOTAL_CUSTOMER_OVERDUE"] / (
        tmp1["TOTAL_CUSTOMER_DEBT"] + 1e-3
    )
    bureau = tmp1

    bureau.to_csv(output_path, index=False)


if __name__ == "__main__":
    add_bureau_features()
