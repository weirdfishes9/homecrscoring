from sklearn.datasets import load_digits
from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, roc_auc_score

import pandas as pd
import neptune.new as neptune
import neptune.new.integrations.sklearn as npt_utils

# Snapshot all .dvc files from any directory
run = neptune.init(
    project="tacobella/HomeCreditScoring",
    api_token="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vYXBwLm5lcHR1bmUuYWkiLCJhcGlfdXJsIjoiaHR0cHM6Ly9hcHAubmVwdHVuZS5haSIsImFwaV9rZXkiOiI3ZDE4OWNhNC03Njc2LTRhZTAtOGFiOC01ZGE2ZTBhMGExNWUifQ==", # noqa
    source_files=[".dvc"],
)  # your credentials

parameters = {
    "n_estimators": 90,
    "learning_rate": 0.07,
    "min_samples_split": 2,
    "min_samples_leaf": 5,
}
run["parameters"] = parameters

gbc = GradientBoostingClassifier(**parameters)
lrc = LogisticRegressionCV(max_iter=500, cv=StratifiedKFold())
df = pd.read_csv("data/train_modified_ohe.csv")
y = df["TARGET"]
df.drop(["TARGET"], axis=1, inplace=True)
X_train, X_test, y_train, y_test = train_test_split(df, y, test_size=0.2, stratify=y)
scaler = StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)
gbc.fit(X_train, y_train)
lrc.fit(X_train, y_train)

# Neptune integration with scikit-learn works with
# the regression and classification problems as well.
# Check the user guide in the documentation for more details:
# https://docs.neptune.ai/integrations-and-supported-tools/model-training/sklearn
run["classifier/gbc"] = npt_utils.create_classifier_summary(
    gbc, X_train, X_test, y_train, y_test
)
run["classifier/lrc"] = npt_utils.create_classifier_summary(
    lrc, X_train, X_test, y_train, y_test
)

run["metrics/gbc/f1_score"] = f1_score(gbc.predict(X_test), y_test)
run["metrics/gbc/roc_auc_score"] = roc_auc_score(gbc.predict(X_test), y_test)
run["metrics/lrc/f1_score"] = f1_score(lrc.predict(X_test), y_test)
run["metrics/lrc/roc_auc_score"] = roc_auc_score(lrc.predict(X_test), y_test)

# track folder
run["train_dataset"].track_files("./data/train_modified_ohe.csv")

run.stop()
