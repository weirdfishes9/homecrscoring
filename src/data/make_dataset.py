import pandas as pd
import click
from typing import List


FEATURES_TO_ENCODE = [
    "CODE_GENDER",
    "NAME_EDUCATION_TYPE",
    "ORGANIZATION_TYPE",
    "OWN_CAR_AGE",
]


def improved_ohe(input_df: pd.DataFrame, columns: List):
    """One Hot Encoding with removing minimal class column.

    :param input_df: Input DataFrame.
    :type input_df: pd.DataFrame
    :param columns: Columns to encode.
    :type columns: List
    :return: DataFrame with OHE encoded columns.
    :type return: pd.DataFrame
    """
    dfs = []
    for col in columns:
        df = pd.get_dummies(
            pd.DataFrame(input_df[col]),
            columns=[col],
            dummy_na=bool(input_df[col].isna().sum()),
        )
        df.drop(
            df[df.columns[df.columns.str.startswith(col)]].sum().idxmin(),
            axis=1,
            inplace=True,
        )
        dfs.append(df)
    return pd.concat(dfs, axis=1)


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def make_dataset(input_path: str, output_path: str):
    """Creating processed DataFrame for model input.

    :param input_path: Path to folder with featured data files.
    :type input_path: str
    :param output_path: Path to save processed DataFrame.
    :type output_path: str
    :return:
    """

    df_train = pd.read_csv(
        f"{input_path}preprocessed_train.csv",
        dtype={"SK_ID_CURR": "object", "OWN_CAR_AGE": "object"},
    )

    df_train = pd.concat(
        [df_train, improved_ohe(df_train[FEATURES_TO_ENCODE], FEATURES_TO_ENCODE)],
        axis=1,
        join="inner",
    )
    df_train.drop(FEATURES_TO_ENCODE, axis=1, inplace=True)

    df_bureau = pd.read_csv(
        f"{input_path}featured_bureau.csv", dtype={"SK_ID_CURR": "object"}
    )

    data = df_bureau.merge(df_train, on=["SK_ID_CURR"], how="inner")

    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    make_dataset()
