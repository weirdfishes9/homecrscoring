import pandas as pd
import click
from sklearn.preprocessing import KBinsDiscretizer


BEST_FEATURES = [
    "AMT_REQ_CREDIT_BUREAU_MON",
    "CODE_GENDER",
    "DAYS_EMPLOYED",
    "DEF_30_CNT_SOCIAL_CIRCLE",
    "DEF_60_CNT_SOCIAL_CIRCLE",
    "EXT_SOURCE_2",
    "EXT_SOURCE_3",
    "FLAG_OWN_REALTY",
    "NAME_CONTRACT_TYPE",
    "NAME_EDUCATION_TYPE",
    "ORGANIZATION_TYPE",
    "OWN_CAR_AGE",
    "REG_CITY_NOT_LIVE_CITY",
    "REG_CITY_NOT_WORK_CITY",
    "REG_REGION_NOT_WORK_REGION",
]


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def preprocessing(input_path: str, output_path: str):
    """Initial preprocessing of raw data.

    :param input_path: Path to application_train dataset.
    :type input_path: str
    :param output_path: Path to save preprocessed DataFrame.
    :type output_path: str
    :return:
    """
    df_train = pd.read_csv(input_path, dtype={"SK_ID_CURR": "object"})

    df_train = df_train[["SK_ID_CURR", "TARGET"] + BEST_FEATURES]

    df_train["AMT_REQ_CREDIT_BUREAU_MON"].fillna(
        df_train["AMT_REQ_CREDIT_BUREAU_MON"].median(), inplace=True
    )
    df_train["DEF_30_CNT_SOCIAL_CIRCLE"].fillna(
        df_train["DEF_30_CNT_SOCIAL_CIRCLE"].median(), inplace=True
    )
    df_train["DEF_60_CNT_SOCIAL_CIRCLE"].fillna(
        df_train["DEF_60_CNT_SOCIAL_CIRCLE"].median(), inplace=True
    )
    df_train["EXT_SOURCE_2"].fillna(df_train["EXT_SOURCE_2"].median(), inplace=True)
    df_train["EXT_SOURCE_3"].fillna(df_train["EXT_SOURCE_2"].median(), inplace=True)

    estimator = KBinsDiscretizer(n_bins=4, encode="ordinal", strategy="kmeans")
    estimator.fit(df_train[["OWN_CAR_AGE"]].dropna())
    df_train["OWN_CAR_AGE"] = pd.cut(
        df_train["OWN_CAR_AGE"],
        bins=estimator.bin_edges_[0],
        ordered=False,
        labels=["1", "2", "3", "4"],
        include_lowest=True,
    )

    df_train["FLAG_OWN_REALTY"] = df_train["FLAG_OWN_REALTY"].apply(
        lambda x: 1 if x == "Y" else 0
    )
    df_train["NAME_CONTRACT_TYPE"] = df_train["NAME_CONTRACT_TYPE"].apply(
        lambda x: 1 if x == "Cash loans" else 0
    )

    df_train["ORGANIZATION_TYPE"] = df_train["ORGANIZATION_TYPE"].apply(
        lambda x: x[: x.find(":")] if x.find(":") > 0 else x
    )
    df_train["ORGANIZATION_TYPE"] = df_train["ORGANIZATION_TYPE"].apply(
        lambda x: x[: x.find(" Type")] if x.find(" Type") > 0 else x
    )
    main_orgs = df_train["ORGANIZATION_TYPE"].value_counts()[:14]
    df_train["ORGANIZATION_TYPE"] = df_train["ORGANIZATION_TYPE"].apply(
        lambda x: x if x in main_orgs else "Other"
    )

    df_train.to_csv(output_path, index=False)


if __name__ == "__main__":
    preprocessing()
