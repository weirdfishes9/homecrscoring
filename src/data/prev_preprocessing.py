import pandas as pd


def improved_ohe(df, columns):
    df = pd.get_dummies(df, columns=columns, dummy_na=True)

    for col in columns:
        df.drop(
            df[df.columns[df.columns.str.startswith(col)]].sum().idxmin(),
            axis=1,
            inplace=True,
        )
    return df


def preprocessing(df):
    df_train = df
    df_train["AMT_ANNUITY"].fillna(df_train["AMT_ANNUITY"].median(), inplace=True)
    df_train["AMT_GOODS_PRICE"].fillna(
        df_train["AMT_GOODS_PRICE"].median(), inplace=True
    )

    df_train["EXT_SOURCE_1"] = df_train["EXT_SOURCE_1"].notna()
    df_train["EXT_SOURCE_2"].fillna(df_train["EXT_SOURCE_2"].median(), inplace=True)
    df_train["FLAG_EXT_SOURCE_3"] = df_train["EXT_SOURCE_3"].notna()
    df_train["EXT_SOURCE_3"].fillna(df_train["EXT_SOURCE_2"].median(), inplace=True)

    # Убираем AVG и MODE, а из колонок с MEDI делаем бинарные признаки
    df_train.drop(df_train.iloc[:, 44:72].columns, axis=1, inplace=True)
    df_train = df_train.rename(
        columns=dict(
            zip(
                df_train.columns[44:63],
                list(map(lambda x: x[:-5], df_train.iloc[:, 44:63].columns)),
            )
        )
    )
    for col in df_train.columns[44:63]:
        df_train[col] = df_train[col].notna()

    df_train.drop(df_train.iloc[:, 44:62].columns, axis=1, inplace=True)
    df_train = df_train.rename(columns={"EMERGENCYSTATE": "FLAG_APARTMENTS_INFO"})

    df_train["DAYS_LAST_PHONE_CHANGE"].fillna(
        df_train["DAYS_LAST_PHONE_CHANGE"].median(), inplace=True
    )

    df_train["OBS_30_CNT_SOCIAL_CIRCLE"].fillna(
        df_train["OBS_30_CNT_SOCIAL_CIRCLE"].median(), inplace=True
    )
    df_train["DEF_30_CNT_SOCIAL_CIRCLE"].fillna(
        df_train["DEF_30_CNT_SOCIAL_CIRCLE"].median(), inplace=True
    )
    df_train["OBS_60_CNT_SOCIAL_CIRCLE"].fillna(
        df_train["OBS_60_CNT_SOCIAL_CIRCLE"].median(), inplace=True
    )
    df_train["DEF_60_CNT_SOCIAL_CIRCLE"].fillna(
        df_train["DEF_60_CNT_SOCIAL_CIRCLE"].median(), inplace=True
    )

    df_train.drop(
        [
            "AMT_REQ_CREDIT_BUREAU_HOUR",
            "AMT_REQ_CREDIT_BUREAU_DAY",
            "AMT_REQ_CREDIT_BUREAU_QRT",
        ],
        axis=1,
        inplace=True,
    )

    # Создаём флаг, показывающий, есть ли информация по бюро
    df_train["FLAG_BUREAU_INFO"] = df_train["AMT_REQ_CREDIT_BUREAU_WEEK"].notna()
    df_train["AMT_REQ_CREDIT_BUREAU_WEEK"].fillna(
        df_train["AMT_REQ_CREDIT_BUREAU_WEEK"].median(), inplace=True
    )
    df_train["AMT_REQ_CREDIT_BUREAU_MON"].fillna(
        df_train["AMT_REQ_CREDIT_BUREAU_MON"].median(), inplace=True
    )
    df_train["AMT_REQ_CREDIT_BUREAU_YEAR"].fillna(
        df_train["AMT_REQ_CREDIT_BUREAU_YEAR"].median(), inplace=True
    )

    df_train["CNT_FAM_MEMBERS"].fillna(
        df_train["CNT_FAM_MEMBERS"].median(), inplace=True
    )

    # Делаем биннинг переменной на основе бинов, полученных выше
    df_train["OWN_CAR_AGE"] = df_train["OWN_CAR_AGE"].apply(
        lambda x: "1" if x < 10 else "2" if x < 17 else "3" if x >= 17 else "0"
    )

    return df_train
