"""Implementation of Add and Add-Del feature selection algorithms."""
import pandas as pd
from typing import Set, Tuple
from tqdm import tqdm
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from src.data.preprocessing import improved_ohe


def best_pair(
    X_train: pd.DataFrame,
    X_test: pd.DataFrame,
    y_train: pd.DataFrame,
    y_test: pd.DataFrame,
    random_state: int,
) -> Set:
    """Finding pair of features with the biggest contribution
    according to f1-score.

    :param X_train: Train objects.
    :type X_train: pd.DataFrame
    :param X_test: Test objects.
    :type X_test: pd.DataFrame
    :param y_train: Train answers.
    :type y_train: pd.DataFrame
    :param y_test: Test answers.
    :type y_test: pd.DataFrame
    :param random_state: Random state.
    :type random_state: int
    :return: Pair of best features.
    :rtype: Set
    """
    best_features = set()
    best_score = 0
    checked_features = set()
    for feature_1 in tqdm(set(X_train.columns)):
        checked_features.add(feature_1)
        for feature_2 in set(X_train.columns) - checked_features:
            tmp_train = X_train[[feature_1, feature_2]]
            tmp_test = X_test[[feature_1, feature_2]]
            for column in (feature_1, feature_2):
                if tmp_train[column].dtype == "object":
                    tmp_train = improved_ohe(tmp_train, [column])
                    tmp_test = improved_ohe(tmp_test, [column])
            scaler = StandardScaler().fit(tmp_train)
            tmp_train = scaler.transform(tmp_train)
            tmp_test = scaler.transform(tmp_test)
            clf = LogisticRegression(random_state=random_state).fit(tmp_train, y_train)
            y_pred = clf.predict(tmp_test)
            score = f1_score(y_pred, y_test)
            if score > best_score:
                best_score = score
                best_features = {feature_1, feature_2}
    return best_features


def Add(
    X_train: pd.DataFrame,
    X_test: pd.DataFrame,
    y_train: pd.DataFrame,
    y_test: pd.DataFrame,
    random_state: int,
    best_features: Set,
    best_score: float,
) -> Tuple[Set, float]:
    """Add algorithm for feature selection.

    :param X_train: Train objects.
    :type X_train: pd.DataFrame
    :param X_test: Test objects.
    :type X_test: pd.DataFrame
    :param y_train: Train answers.
    :type y_train: pd.DataFrame
    :param y_test: Test answers.
    :type y_test: pd.DataFrame
    :param random_state: Random state.
    :type random_state: int
    :param best_features: Best features before function calling.
    :type best_features: Set
    :param best_score: Best score before function calling.
    :type best_score: float
    :return: Best set of features and score on this features.
    :rtype: Tuple[Set, float]
    """
    lower_bound = len(best_features)
    upper_bound = X_train.shape[1]
    while lower_bound < upper_bound:
        print("Add iteration\n", "Current number of features ", lower_bound)
        scores = {}
        for feature in set(X_train.columns) - best_features:
            current_columns = best_features | {feature}
            tmp_train = X_train[list(current_columns)]
            tmp_test = X_test[list(current_columns)]
            for column in current_columns:
                if tmp_train[column].dtype == "object":
                    tmp_train = improved_ohe(tmp_train, [column])
                    tmp_test = improved_ohe(tmp_test, [column])
            scaler = StandardScaler().fit(tmp_train)
            tmp_train = scaler.transform(tmp_train)
            tmp_test = scaler.transform(tmp_test)
            clf = LogisticRegression(max_iter=500, random_state=random_state).fit(
                tmp_train, y_train
            )
            y_pred = clf.predict(tmp_test)
            scores[tuple(current_columns)] = f1_score(y_pred, y_test)
        current_best_features = max(scores, key=scores.get)
        current_best_score = scores[current_best_features]
        if current_best_score - best_score > 1e-6:
            best_score = current_best_score
            best_features = set(current_best_features)
        else:
            break
        print("Features: ", best_features)
        print("Score: ", best_score)
        lower_bound += 1
    print("Add finished")
    return best_features, best_score


def Del(
    X_train: pd.DataFrame,
    X_test: pd.DataFrame,
    y_train: pd.DataFrame,
    y_test: pd.DataFrame,
    random_state: int,
    best_features: Set,
    best_score: float,
) -> Tuple[Set, float]:
    """Del algorithm for feature selection.

    :param X_train: Train objects.
    :type X_train: pd.DataFrame
    :param X_test: Test objects.
    :type X_test: pd.DataFrame
    :param y_train: Train answers.
    :type y_train: pd.DataFrame
    :param y_test: Test answers.
    :type y_test: pd.DataFrame
    :param random_state: Random state.
    :type random_state: int
    :param best_features: Best features before function calling.
    :type best_features: Set
    :param best_score: Best score before function calling.
    :type best_score: float
    :return: Best set of features and score on this features.
    :rtype: Tuple[Set, float]
    """
    upper_bound = len(best_features)
    while upper_bound > 1:
        print("Del iteration\n", "Current number of features ", upper_bound)
        scores = {}
        for feature in best_features:
            current_columns = best_features - {feature}
            tmp_train = X_train[list(current_columns)]
            tmp_test = X_test[list(current_columns)]
            for column in current_columns:
                if tmp_train[column].dtype == "object":
                    tmp_train = improved_ohe(tmp_train, [column])
                    tmp_test = improved_ohe(tmp_test, [column])
            scaler = StandardScaler().fit(tmp_train)
            tmp_train = scaler.transform(tmp_train)
            tmp_test = scaler.transform(tmp_test)
            clf = LogisticRegression(max_iter=500, random_state=random_state).fit(
                tmp_train, y_train
            )
            y_pred = clf.predict(tmp_test)
            scores[tuple(current_columns)] = f1_score(y_pred, y_test)
        current_best_features = max(scores, key=scores.get)
        current_best_score = scores[current_best_features]
        if current_best_score - best_score > 1e-6:
            best_score = current_best_score
            best_features = set(current_best_features)
        else:
            break
        print("Features: ", best_features)
        print("Score: ", best_score)
        upper_bound -= 1
    print("Del finished")
    return best_features, best_score


def add_del(
    X_train: pd.DataFrame,
    X_test: pd.DataFrame,
    y_train: pd.DataFrame,
    y_test: pd.DataFrame,
    random_state: int,
) -> Set:
    """Add-Del algorithm for feature selection.

    :param X_train: Train objects.
    :type X_train: pd.DataFrame
    :param X_test: Test objects.
    :type X_test: pd.DataFrame
    :param y_train: Train answers.
    :type y_train: pd.DataFrame
    :param y_test: Test answers.
    :type y_test: pd.DataFrame
    :param random_state: Random state.
    :type random_state: int
    :return: Set of best features.
    :rtype: Set
    """
    print("Finding best pair of features")
    best_features = best_pair(X_train, X_test, y_train, y_test, random_state)
    print("Best pair: ", best_features)
    best_score = 0
    while 1:
        best_features, best_score = Add(
            X_train, X_test, y_train, y_test, random_state, best_features, best_score
        )
        best_features_num = len(best_features)
        best_features, best_score = Del(
            X_train, X_test, y_train, y_test, random_state, best_features, best_score
        )
        if best_features_num == len(best_features):
            break
    print("The best features founded! ", best_features)
    return best_features
